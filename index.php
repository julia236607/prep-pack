<?php
   include("bd.php");
   session_start();
   
   if($_SERVER["REQUEST_METHOD"] == "POST") {
      // username and password sent from form 
      
      $myusername = mysqli_real_escape_string($db,$_POST['username']);
      $mypassword = mysqli_real_escape_string($db,$_POST['password']); 
      
      $sql = "SELECT id FROM members WHERE username = '$myusername' and password = '$mypassword'";
      $result = mysqli_query($db,$sql);
      $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
      $active = $row['active'];
      
      $count = mysqli_num_rows($result);
      
      // If result matched $myusername and $mypassword, table row must be 1 row
		
      if($count == 1) {
         session_register("myusername");
         $_SESSION['login_user'] = $myusername;
         
         header("location: profile.php");
      }else {
         $error = "Your Login Name or Password is invalid";
      }
   }
?>
<!DOCTYPE html>
<head>
	<meta charset="utf-8">
	<title>	Project</title> 
	<script src="js/jquery-3.2.1.js"></script>
	  <meta name="viewport" content="width=width-device, initial-scale=1">
	<script src="maskinput.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="style.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Rubik">
	<link rel="stylesheet" type="text/css" href="animate.min.css">
	<script src="js/wow.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<style>

		.topnav {
		  overflow: hidden;
		  padding: 10px;
		    
		}

		.topnav a {
		  float: left;
		  display: block;
		  color: white;
		  text-align: center;
		  padding: 1px 10px 1px 10px;
		   text-decoration: none;
		  font-size: 17px;
		}

		.topnav a:hover {
		  color: green;
		}

		.activea {
		  background-color: #4CAF50;
		  color: white;
		}

		.topnav .icon {
		  display: none;
		}

		@media screen and (max-width: 600px) {
		  .topnav a:not(:first-child) {display: none;}
		  .topnav a.icon {
		    float: right;
		    display: block;
		    color: black;
		  }
		  .topnav{
		  	background-color: white;
		  }
		  .center{
		  	margin-left:0;
		  }
		}

		@media screen and (max-width: 600px) {
		  .topnav.responsive {position: relative;}
		  .topnav.responsive .icon {
		    position: absolute;
		    right: 0;
		    top: 0;
		     background-color: white;
		  }

		  .topnav.responsive a {
		    float: none;
		    display: block;
		    text-align: left;
		        color: black;
		        padding: 14px 16px;
		  }
		.activea {
			display: none;
		}
		#modal-box {
			width:100%;
		}
		}
		@media screen and (max-width: 600px){

		  #wrapper2 {
		    margin-left: 10%;
		  }
		}
		/*******************************************************/
		                /*ADAPTIVE  FIRST BLOCK*/
		/*****************************************************/
		@media (min-width: 992px) {
		  .col-md-1-5 { width: 20%; }
		  .col-md-2-5 { width: 40%; }
		  .col-md-3-5 { width: 60%; }
		  .col-md-4-5 { width: 80%; }
		  .col-md-5-5 { width: 100%; }

		}

		@media (min-width: 1024px) {
		  .col-lg-1-5 { width: 20%; }
		  .col-lg-2-5 { width: 40%; }
		  .col-lg-3-5 { width: 60%; }
		  .col-lg-4-5 { width: 80%; }
		  .col-lg-5-5 { width: 100%; }
		}
.footer_name a{
	color:black;
}

</style>
</head>

<body >
	<!--preloader-->
	<div id='wrap_preloader'>
	    <img src="images/2.gif">
	</div>
	<!--Header-->
	<div id="video-container">		
		<div class="topnav" id="myTopnav">
			<div class="activea">
				<a href="#logo"><img  width="80px" src="bg/logo.png"></a>
				<a id="button" href="#home">Lorem ipsum</a>
			</div>
		<div class="center">
			<a href="#service">Услуги</a>
			<a href="#fbm">FBM</a>
			<a href="#price">Цены</a>
			<a href="#about">О нас</a>
			<a href="#comments">Отзывы</a>
			<a href="#feedback">Контакты</a>
			<a href="#news">Новости</a>
		</div>
		<a  onclick="document.getElementById('id01').style.display='block'" style="width:auto; float:right;cursor: pointer;">Войти</a>
		<a  style="float:right;color:white;" id="button" class=" oval_border" href="#online">Онлайн заявка</a>
		<a  style="float:right;margin-top:0" id="button" class=" square_border" href="#en">EN</a>
  		<a href="javascript:void(0);" style="font-size:35px;" class="icon" onclick="myFunction()">&#9776;</a>
		</div>
		<!--modal LOGIN-->
		<div id="id01" class="modal">
			<div id="modal-box">
			    <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>
			    <div id="tabbox">
			        <a href="#" id="signup" class="tab signup"><h4>Регистрация</h4></a>
			        <a href="#" id="login" class="tab "><h4>Войти</h4></a>
			    </div>
			    <div id="panel">
			    	<form action="register.php" method="post">
				        <div id="signupbox">
				            <h1>Регистрация</h1>
				            <input type="text" placeholder="Имя" name="username" required>
				            <input id="phone-input" type="tel" value="" name="PhoneIn" aria-label="Please enter your phone number" placeholder="+X(XXX)-XXX-XXXX">
				            <input type="email" placeholder="E-mail" name="email"  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required>
				            <input type="password" placeholder="Пароль" name="password" required>
				            <input type="password" placeholder="Повторите пароль" name="password1" required>
				            <button type="submit">Регистрация</button>
				        </div>
			    	</form>
			    	<form action="login.php" method="post">
				        <div id="loginbox">
				            <h1>Войти</h1>
				            <input type="text" placeholder="Имя" name="username" required>
				            <input type="password" placeholder="Пароль" name="password" required>
				            <!--<a href="#" class="forgot-button">Забыли пароль?</a>-->
				            <button type="submit">Войти</button>
				        </div>
			        </form>
			     <!-- <div id="forgetbox">
			            <h1>Forgot Password</h1>
			            <input type="text" placeholder="Enter Email" name="email" required>
			            <button type="submit">Submit</button>
			        </div>-->
			    </div>
			</div>
		</div>
			<!--END MODAL-->
		<div class="video-overlay">
			<div class="video-content">
				<div class="inner">
				  	<h1 style="color:white; text-align: center;">Lorem ipsum dolor sit amet</h1>
				</div>
			</div>
			<video autoplay loop src="bg/bg.mp4"></video>
		</div>
		<a href="#section1" class="arrow-down">
			<img src="bg/arrow.png">
		</a>
	</div>
	<!--Header content -->
	<div class="white" style="overflow-x: hidden;">
		<div class="container"  id="service">
			<div class="row">
				<div class="center-block  ">
			  		<h1>Lorem ipsum dolor <br> sit amet, conse</h1>
			  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id sed voluptatem error vel eius placeat harum </br>
			  		voluptate doloribus ratione in, vero aliquid et, itaque, repellat magnam nisi adipisci unde, odit esse architecto hic! </br>
			  		Eaque libero officia laudantium laboriosam non velit vero pariatur</p>
			    </div>
			</div>
			<div class="row show-grid ">
			    <div class="col-sm-6 col-md-4-5 col-lg-1-5  center-block">
			        <img src="images/pre-shipment.png">
			        <h3 class="titles">PRE-SHIPMENT</h3>
			        <p>Фото и видеосъемка</br>
			           Обработка фото</br>
			           Инфографика</p>
			    </div>
			    <div class="col-sm-6 col-md-5-5 col-lg-1-5  center-block">
			        <img src="images/delivery.png" >
			        <h3 class="titles">ДОСТАВКА</h3>
			        <p>Услуги отправки товара</br>
			           прямиком к вашему дому</br>
			           покупателю, с вашего преп</br>
			           центра по самым</br>
			           привлекательным </br>
			           почтовым тарифам</br></p>
			    </div>
			    <div class="col-sm-6 col-md-3-5 col-lg-1-5  center-block">
			          <img src="images/pack.png" >
			          <h3 class="titles">PREP AND PACK</h3>
			          <p>Консолидация</br>
			             Создания наборов</br>
			             (set-bundle)</br>
			             Маркировка</br>
			             Упаковка в пузырьч. пленку</br>
			             Комплектация доп. материалами</br>
			             Упаковка от А до Я</br></p>
			    </div>
			    <div class="col-sm-6 col-md-2-5 col-lg-1-5  center-block">
			          <img src="images/storage.png">
			          <h3 class="titles">ХРАНЕНИЕ</h3>
			          <p>Долосрочное хранение</br>
			             Краткосрочное хранение</br>
			             Частичное пополнение</br>
			             складских запасов</br>
			             AMAZON</br></p>
			    </div>
			    <div class="col-sm-6 col-md-4-5 col-lg-1-5  center-block">
			          <img src="images/plane.png">
			          <h3 class="titles">OTHER</h3>
			          <p>Международные перевозки:</br>
			             Канада</br>
			             Украина</br>
			             Молдова</br>
			             Россия</br></p>
			    </div>
			</div>
		</div>
	</div>
<!--Service-->

</div>
	<!--Service-->
	<div class="service" id="price">
		<div class="container">
			<div class="row">
				<div class="center-block ">
					<h1>Стоимость услуг</h1>
				    <p>Мы разработали четыре простых тарифа для наших клиентов.</br>
				       Если вы затрудняетесь с выбором - <span ><a style="color: #ff9900;">оставьте заявку</a></span>, и мы поможем вам</br>
				       подобрать тариф, наиболее подходящий вашему бизнесу.</p>
				</div>
			</div>
				<div class="row center-block" id="wrapper2" style="margin-right:0;">
			         <div id="div3" class="col-md-3 col-xs-12  block_service center-block element2 wow fadeInDown" data-wow-delay="0.3s">
			            <div class="title"><h6>Lorem ipsum dolor sit - 1$</h6></div>
						 <div ><h1>Standart</h1></div>
						<hr class="line">
			            <p>Lorem ipsum dolor sit amet,consectetur adipisicing elit. 
			            Id sed voluptatem error vel eius placeat harum voluptate
			             doloribus ratione in, vero aliqui</p>
			          <button  class="slideRight" data-toggle="modal" data-target="#standart"><h6>Узнать подробнее</h6></button>
			        </div>
	 				 <!-- Modal -->
					  <div class="modal fade" id="standart" role="dialog">
					    <div class="modal-dialog">
					      <!-- Modal content-->
					      <div class="modal-content">
					        <div class="modal-header">
					          <h3 class="modal-title">Standart</h3>
					        </div>
					        <div class="modal-body">
					           <p>Lorem ipsum dolor sit amet,consectetur adipisicing elit. 
							            Id sed voluptatem error vel eius placeat harum voluptate
							             doloribus ratione in, vero aliqui</p>
					        </div>
					        <div class="modal-footer">
					          <a  class="btn btn-default " data-dismiss="modal">Close</a>
					        </div>
					      </div>
					      
					    </div>
					  </div>
			   	  <!-- End Modal --> 
			          <div id="div3" class="col-md-3 col-xs-6 clearfix block_service center-block element2 wow fadeInDown" data-wow-delay="0.6s">
			            <div class="title"><h6>Lorem ipsum dolor sit - 1$</h6></div>
						 <div ><h1>Online арбитаж</h1></div>
						<hr class="line">
			            <p>Lorem ipsum dolor sit amet,consectetur adipisicing elit. 
			            Id sed voluptatem error vel eius placeat harum voluptate
			             doloribus ratione in, vero aliqui</p>
			          <button  class="slideRight" data-toggle="modal" data-target="#online"><h6>Узнать подробнее</h6></button>
			        </div>
			         <!-- Modal -->
					  <div class="modal fade" id="online" role="dialog">
					    <div class="modal-dialog">
					      <!-- Modal content-->
					      <div class="modal-content">
					        <div class="modal-header">
					          <h3 class="modal-title">Online арбитаж</h3>
					        </div>
					        <div class="modal-body">
					           <p>Lorem ipsum dolor sit amet,consectetur adipisicing elit. 
							            Id sed voluptatem error vel eius placeat harum voluptate
							             doloribus ratione in, vero aliqui</p>
					        </div>
					        <div class="modal-footer">
					          <a  class="btn btn-default " data-dismiss="modal">Close</a>
					        </div>
					      </div>
					      
					    </div>
					  </div>
			   	  <!-- End Modal --> 
			          <div id="div2" class="col-md-3 col-xs-6 clearfix block_service center-block element2 wow fadeInDown" data-wow-delay="0.9s" ">
			            <div class="title"><h6>Lorem ipsum dolor sit - 1$</h6>
			            <h6>Lorem ipsum - 2$</h6>
			            </div>
						 <div ><h1>Хранение</h1></div>
						<hr class="line">
			            <p>Lorem ipsum dolor sit amet,consectetur adipisicing elit. 
			            Id sed voluptatem error vel eius placeat harum voluptate 
			            doloribus ratione in, vero aliqui</p>
			           <button  class="slideRight" data-toggle="modal" data-target="#storage"><h6>Узнать подробнее</h6></button>
			        </div>
			         <!-- Modal -->
					  <div class="modal fade" id="storage" role="dialog">
					    <div class="modal-dialog">
					      <!-- Modal content-->
					      <div class="modal-content">
					        <div class="modal-header">
					          <h3 class="modal-title">Хранение</h3>
					        </div>
					        <div class="modal-body">
					           <p>Lorem ipsum dolor sit amet,consectetur adipisicing elit. 
							            Id sed voluptatem error vel eius placeat harum voluptate
							             doloribus ratione in, vero aliqui</p>
					        </div>
					        <div class="modal-footer">
					          <a  class="btn btn-default " data-dismiss="modal">Close</a>
					        </div>
					      </div>
					      
					    </div>
					  </div>
			   	  <!-- End Modal --> 
			          <div  id="div1" class="col-md-3 col-xs-6 clearfix block_service center-block element2 wow fadeInDown" data-wow-delay="1.2s" ">
			            <div class="title"><h6>Lorem ipsum dolor sit - 1$</h6></div>
						 <div ><h1>FBM</h1></div>
						<hr class="line">
			            <p>Lorem ipsum dolor sit amet,consectetur adipisicing elit. 
			            Id sed voluptatem error vel eius placeat harum voluptate 
			            doloribus ratione in, vero aliqui</p>
			            <button  class="slideRight" data-toggle="modal" data-target="#fbm"><h6>Узнать подробнее</h6></button>
			        </div>
			         <!-- Modal -->
					  <div class="modal fade" id="fbm" role="dialog">
					    <div class="modal-dialog">
					      <!-- Modal content-->
					      <div class="modal-content">
					        <div class="modal-header">
					          <h3 class="modal-title">FBM</h3>
					        </div>
					        <div class="modal-body">
					           <p>Lorem ipsum dolor sit amet,consectetur adipisicing elit. 
							            Id sed voluptatem error vel eius placeat harum voluptate
							             doloribus ratione in, vero aliqui</p>
					        </div>
					        <div class="modal-footer">
					          <a  class="btn btn-default " data-dismiss="modal">Close</a>
					        </div>
					      </div>
					      
					    </div>
					  </div>
			   	  <!-- End Modal --> 
			    </div>
			        <div class="container">
				    <div class="row">
						<div class="center-block">
							<h4>В каждый тариф включено:</h4>
							</div>
							<div class="row" style="margin-left: 8%">
							<div class="col-md-3  col-xs-6 clearfix float_left" data-tag='web'>
								<p class="float_left"><img src="images/tick.png" width="20px">Прием товара</p></br>
								<p class="float_left"><img src="images/tick.png" width="20px">Инспекция</p>
							</div>
							<div class="col-md-3 col-xs-6  clearfix float_left" data-tag='web'>
								<p class="float_left"><img src="images/tick.png" width="20px">Маркировка</p></br>
								<p class="float_left"><img src="images/tick.png" width="20px">Фотоотчет</p>
							</div>
							<div class="col-md-3 col-xs-6  clearfix float_left" data-tag='web'>
								<p class="float_left"><img src="images/tick.png" width="20px">Консолидация</p></br>
								<p class="float_left"><img src="images/tick.png" width="20px">Пакетирование</p>
							</div>
							<div class="col-md-3 col-xs-6  clearfix float_left" data-tag='web'>
								<p class="float_left"><img src="images/tick.png" width="20px">Декомплектация</br>
							(брошуры-запчасти и тп)</p>
							</div>
							</div>
				    </div>
				</div>
		</div> 

</div>

<!--Advantages-->
	<div class="white">
		<div class="container">
			<div class="row">
				<div class="center-block width_half" >
			  		<h1>Наши преимущества</h1>
			  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id sed voluptatem error vel eius placeat harum </br>
			  		voluptate doloribus ratione in, vero aliquid et, itaque, repellat magnam nisi adipisci unde, odit esse architecto hic! </br>
			  		Eaque libero officia laudantium laboriosam non velit vero pariatur</p>
			    </div>
			</div>
			<div class="row show-grid ">
			    <div class="col-sm-6 col-md-4-5 col-lg-1-5  center-block ">
			        <img src="images/message.png">
			        <h3 class="titles">КОММУНИКАЦИЯ</h3>
			        <p>Мультиязычное общение с</br>
			           клиентами на его</br>
			           языке, что крайне</br>
			           важно для понимания</br>
			           всех аспектов.</p>
			    </div>
			     <div class="col-sm-6 col-md-4-5 col-lg-1-5  center-block">
			          <img src="images/fb.png">
			          <h3 class="titles">ПОДДЕРЖКА</h3>
			          <p>Дружелюбная группа в</br>
			             Facebook с опытными</br>
			             участниками, которые могут</br>
			             помочь и подсказать</br>
			             решение любого вопроса.</p>
			    </div>
			    <div class="col-sm-6 col-md-5-5 col-lg-1-5  center-block">
			        <img src="images/user.png" >
			        <h3 class="titles">ЛИЧНЫЙ КАБИНЕТ</h3>
			        <p>Удобный и функциональный</br>
			           личный онлайн кабинет для</br>
			           быстрого оформления и</br>
			           обработки заказов.</p>
			    </div>
			    <div class="col-sm-6 col-md-3-5 col-lg-1-5  center-block">
			          <img src="images/trolley.png" >
			          <h3 class="titles">ДОСТАВКА</h3>
			          <p>Доставка от нашего адреса</br>
			             на ближайший склад</br>
			             AMAZON занимает до 1-го</br>
			             рабочего дня,благодаря</br>
			             нашему удачному</br>
			             расположению.</p>
			    </div>
			    <div class="col-sm-6 col-md-2-5 col-lg-1-5  center-block">
			          <img src="images/shop.png">
			          <h3 class="titles">FBM</h3>
			          <p>Услуга FBM позволяет</br>
			             вам хранить и продавать</br>
			             любые товары дешевле</br>
			             чем на FBA.</p>
			    </div>
			   
			</div>
		</div>
	</div>
<!--About us-->
	<div class="about_us" id="about">
		<div class="container">
				<div class="row">
					<div class="center-block " style="color: white; width:70%;margin-bottom: 40px;">
				  		<h1 style="color: white;">О нас</h1>
						<div class="wrapper ">
						  <a href="#" class="about"> <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id sed voluptatem error vel eius placeat harum voluptate doloribus ratione in, vero aliquid et, itaque, repellat magnam nisi adipisci unde, odit esse architecto hic! Eaque libero officia laudantium laboriosam non velit vero pariatur.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id sed voluptatem error vel eius placeat harum voluptate doloribus ratione in, vero aliquid et, itaque, repellat magnam nisi adipisci unde, odit esse architecto hic! Eaque libero officia laudantium laboriosam non velit vero pariatur
						  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id sed voluptatem error vel eius placeat harum voluptate doloribus ratione in, vero aliquid et, itaque, repellat magnam nisi adipisci unde, odit esse architecto hic! Eaque libero officia laudantium laboriosam non velit vero pariatur.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id sed voluptatem error vel eius placeat harum voluptate doloribus ratione in, vero aliquid et, itaque, repellat magnam nisi adipisci unde, odit esse architecto hic! Eaque libero officia laudantium laboriosam non velit vero pariatur
						  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id sed voluptatem error vel eius placeat harum voluptate doloribus ratione in, vero aliquid et, itaque, repellat magnam nisi adipisci unde, odit esse architecto hic! Eaque libero officia laudantium laboriosam non velit vero pariatur.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id sed voluptatem error vel eius placeat harum voluptate doloribus ratione in, vero aliquid et, itaque, repellat magnam nisi adipisci unde, odit esse architecto hic! Eaque libero officia laudantium laboriosam non velit vero pariatur</p></a>
						</div>
				  		
				    </div>
		</div>
	</div>
	<!--Counter-->
	<div class="white">
		<div class="container">
			<div class="row">
				<div class="center-block " style="">
			  		<h1>Lorem ipsum dolor</h1>
			    </div>
			</div>
			<div class="container">
			  <div class="row">
				  <div id="counter" >
				    <div class="col-md-4 col-sm-4 center-block" >
				     <img src="images/truck.png"  width="60px">
						    <div class="counter" data-count="41410">0</div>
		     			<h5 class="counterBoxDetails">ОТПРАВОК В ТЕКУЩИЙ ГОД</h5>
				    </div>
				    <div class="col-md-4 col-sm-4 center-block">
				    <img src="images/box.png" width="60px">
				      <div class="counter" data-count="124">1</div>
		     		  <h5 class="counterBoxDetails">КОРОБОК КАЖДЫЙ ДЕНЬ</h5>
				    </div>
				    <div class=" col-md-4 col-sm-4 center-block">
				   	<img src="images/paid.png"  width="60px">
				      <div class="counter" data-count="21215">200</div>
		     		  <h5 class="counterBoxDetails">ЕДИНИЦ</h5>
				    </div>
				  </div>
				  </div>
			</div>
		</div>
	</div>
<!--Comments-->
		<div class="comments" id="comments">
			<div class="container">
				<div class="row">
					<div class="center-block " style="">
				  		<h1>Отзывы</h1>

				    </div>
				</div>
			</div>
		</div>
<!--Delivery-->
<div class=" delivery" id="news">
	<div class="container">
			<div class="row">
				<div class="center-block " >
			  		<h1>Экономьте на доставке</h1>
			  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id sed voluptatem error vel eius placeat harum </br>
			  		voluptate doloribus ratione in, vero aliquid et, itaque, repellat magnam nisi adipisci unde, odit esse architecto hic! </br>
			  		Eaque libero officia laudantium laboriosam non velit vero pariatur</p>
			    </div>
			    <div class="row">
			    	<img src="images/ups.png">
			    	<img src="images/usps.png">
			    	<img src="images/dhl.png">
			    	<img src="images/fedex.png">
			    </div>
			</div>
		</div>
</div>
<!--Feedback-->
	<div class="feedback" id="feedback">
		<div class="container">
			<div class="row">
				<div class="center-block" >
			  		<h1>Оставить заявку</h1>
				</div>
				<div>
					<form action= "post.php" method= "POST">      
					  <input name="name" type="text"  class="feedback-input" placeholder="Ваше имя" />   
					  <input name="email" type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" class="feedback-input" placeholder="Email" />
					  <textarea name="message" class="feedback-input" placeholder="Ваш вопрос"></textarea>
					  <input type="submit" value="Отправить"/>
					</form>
				</div>
			</div>
		</div>
	</div>

	<a href="#" id="scrollup">Scroll</a>
	<!--FOOTER-->
	<footer class="site-footer">
	   <div class="container">
		   <div class="row">
			   <div class=" col-md-4 col-sm-4 " style="text-align: left;" >
				   1990 NE 163rd Street #227<br>
				  North Miami Beach, Florida, 33162<br>
				   United States
			   </div>
			 	<div class=" col-md-4 col-sm-6 footer_name" style="text-align: center;" >
				 	<div class="col-md-4 col-sm-4" >
					 	<a href="#about">О нас</a> <br> 
					 	<a href="#contacts">Контакты</a>
				 	</div>
				 	<div class="col-md-4 col-sm-4" >
				 		<a href="#service">Услуги</a> <br> 
					 	<a>Вакансии</a>
				 	</div>
				 	<div class="col-md-4 col-sm-4" >
				 		<a>Портфолио</a><br> 
					 	<a>Партнерам </a>
				 	</div>
			 	</div>
			   <div class=" col-md-4 col-sm-4 " style="text-align: right;"> 
				   customer@multiskills.us<br>
				   (305) 527-9414‬<br>
				   <a><img src="images/instagram.png"></a>
				   <a><img src="images/facebook.png"></a>
				   <a><img src="images/twitter.png"></a>
				   <a>
			    </div>
			   <div class=" col-md-12 col-sm-12 black">&copy 2018 Multiskills</div>
		   </div>
	   </div>
	</footer>

<!--SCRIPTS-->
<!--PRELOADER-->
<script type='text/javascript'>
    $(window).on('load', function () {
      $('#wrap_preloader').delay(1000).fadeToggle(800);
    });
  </script>
<!--MENU-->
<script>

</script>
<!--ARROW DOWN-->
<script type="text/javascript">
	$(function(){
	function loop(){
		$('.arrow-down')
		 .animate({bottom:40},150)
		 .animate({bottom:20},800, loop); // callback	
	};
	loop();
	});
</script>
<!--COUNTER-->
<script type="text/javascript">
	var a = 0;
	$(window).scroll(function() {

	  var oTop = $('#counter').offset().top - window.innerHeight;
	  if (a == 0 && $(window).scrollTop() > oTop) {
	    $('.counter').each(function() {
	      var $this = $(this),
	        countTo = $this.attr('data-count');
	      $({
	        countNum: $this.text()
	      }).animate({
	          countNum: countTo
	        },

	        {

	          duration: 2000,
	          easing: 'swing',
	          step: function() {
	            $this.text(Math.floor(this.countNum));
	          },
	          complete: function() {
	            $this.text(this.countNum);
	          
	          }

	        });
	    });
	    a = 1;
	  }

	});
	</script>
<script type="text/javascript">
	$(document).ready(function(){
	    var visited = {
	        see_1 : false,
	        see_2 : false,
	    };
	    $(document).scroll(function(e){
	       
	        if (!visited.see_2){
	            var coord2 = $("#wrapper2").offset();
	            var top2 = $(window).scrollTop()+$(window).height();
	            if (top2 > coord2.top){
	                visited.see_2 = true;
	                var list2 = $('.element2');
	                var i2=-1;
	                a2 = function(){
	                    if (i2 == list2.length-1)
	                        return;
	                        ++i2;
	                        $(list2[i2]).animate({opacity: 1}, 500, a2);
	                    };
	                a2();
	            }
	            else{
	                return;
	            }
	        }
	    })
	})	
</script>
<!--SCROLL TO ID-->
<script type="text/javascript">
 $(document).ready(function(){
    $("body").on("click","a", function (event) {
        event.preventDefault();
        var id  = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1500);
    });
 });
</script>
<!--SCROLL TO UP-->
<script type="text/javascript">
	$(document).ready(function(){ 

  $(window).scroll(function(){
      if ($(this).scrollTop() > 100) {
          $('#scrollup').fadeIn();
      } else {
          $('#scrollup').fadeOut();
      }
  }); 

  $('#scrollup').click(function(){
      $("html, body").animate({ scrollTop: 0 }, 400);
      return false;
  });

	});
</script>
<script>
 new WOW().init();
</script>
<!--SIGNUP/LOGIN-->
<script type="text/javascript">
		$(document).ready(function() {
	  $(".signup").addClass("login-select");
	  $(".tab").click(function() {
	    var X = $(this).attr("id");

	    if (X == "signup") {
	      $("#login").removeClass("login-select");
	      $("#signup").addClass("login-select");
	      $("#loginbox").hide(300);
	      $("#signupbox").show(300);
		  $("#forgetbox").hide(300);
	    } else {
	      $("#signup").removeClass("login-select");
	      $("#login").addClass("login-select");
	      $("#signupbox").hide(300);
	      $("#loginbox").show(300);
		  $("#forgetbox").hide(300);
	    }
	  });
	  
	  $(".forgot-button").click(function(){
		  $("#signupbox").hide(300);
		  $("#loginbox").hide(300);
		  $("#forgetbox").show(300);
	  });
		});

		// Get the modal
		var modal = document.getElementById('id01');

		// When the user clicks anywhere outside of the modal, close it
		window.onclick = function(event) {
		    if (event.target == modal) {
		        modal.style.display = "none";
		    }
		}
</script>
<!--MASK PHONE-->
<script type="text/javascript">
	function phoneMask() {
	  var num = $(this)
	    .val()
	    .replace(/\D/g, "");
	  $(this).val("+"+
	    num.substring(0, 1) +
	      "(" +
	      num.substring(1, 4) +
	      ")" +
	      num.substring(4, 7) +
	      "-" +
	      num.substring(7, 11)
	  );
	}
	$('[type="tel"]').keyup(phoneMask);
</script>
<!--modal-->
<script type="text/javascript">
	$(document).ready(function(){
    // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
    $('.modal-trigger').leanModal();
  });
</script>
<script>
	function myFunction() {
	    var x = document.getElementById("myTopnav");
	    if (x.className === "topnav") {
	        x.className += " responsive";
	    } else {
	        x.className = "topnav";
	    }
	}
</script>
</body>

</html>
